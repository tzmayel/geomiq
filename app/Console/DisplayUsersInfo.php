<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class DisplayUsersInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'displayUsersInfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Shows users info';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $headers = ['id','name','role','email','company_name','registered_on','last_login'];
        $users = User::with('roles')
            ->join('user_profiles', 'users.id', '=', 'user_profiles.user_id')
            ->get();


        //$users = User::all(['name', 'email'])->toArray();
        $result = array();
        foreach($users as $user){
            // print_r($user);
            $result[] = array(
                $user->id,
                $user->name,
                $user->roles->first()->name,
                $user->email,
                $user->userProfile()->first()->company_name,
                $user->created_at,
                $user->updated_at
            );
        }
        // dd($result);
        $this->info('Tom Mayel is proud to presend :-))))))  this:');
        $this->table($headers, $result);
    }
}
