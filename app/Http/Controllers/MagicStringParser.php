<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MagicStringParser extends Controller
{
    private $data = array(
        'features' => array()
    );

    function updateData($key, $index, $value){

        // make sure that all indexes are numbers
        $index = intval($index);

        if($key == 'elapsed_time') {
            $this->data['elapsed_time'] = $value;
        }
        else if($key == 'type'){
            $this->data['type'] = $value;
        }
        else if($key == 'radius'){
            $this->data['features'][$index]['radius'] = $value;
        }
        else if($key == 'direction'){
            $this->data['features'][$index]['radius'] = $value;
        }
        else if($key == 'position'){
            $this->data['features'][$index]['position'][] = round($value);
        }

        $this->data['feature_count'] = count($this->data['features']);
    }

    //
    function index(Request $request) {
        $str = $name = $request->input('str');

        $elements = preg_split( "/(,|%)/", $str );

        foreach($elements as $element){
            $element = trim($element);
            $key = '';
            $index = '';
            $value = '';
            // print_r($element);

            if(strpos($element, '=') !== false  || strpos($element, '//') !== false){
                $arr = preg_split( "/(=|\/\/)/", $element );

                //last element always is the value
                $value = $arr[1];

                //if there is - it is delimiter for key-index, if not it's only key
                if(substr_count($element, '-') == 1) {
                    list($key, $index) = explode('-', $arr[0]);
                }
                else{
                    $key = $arr[0];
                }

            }
            else{
                // no sign '=' or '%'
                // in this case delimiter is '-'
                if(substr_count($element, '-') == 1) {
                    list($key, $value) = explode('-', $element);
                }
                else if(substr_count($element, '-') == 2) {
                    list($key, $index, $value) = explode('-', $element);
                }
                else{
                    dd('something went wrong for element='.$element);
                }

            }
            $this->updateData($key,$index,$value);
        }

        return [
            "data" => $this->data
        ];
    }
}
