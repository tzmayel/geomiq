This is a Laravel project

1. clone the repo
2. composer install
3. copy .env.example to .env and edit it; configure mysql username, password and database
4. php artisan migrate:fresh
5. php artisan db:seed

## end finally ;-)

For results of PART B - the database query result as console output use
6. php artisan displayUsersInfo

you should see this:
Mayels-MBP:geomiq mss$ php artisan displayUsersInfo
Tom Mayel is proud to presend :-))))))  this:
+----+------------------------+--------+-------------------------------+---------------------------------+---------------------+---------------------+
| id | name                   | role   | email                         | company_name                    | registered_on       | last_login          |
+----+------------------------+--------+-------------------------------+---------------------------------+---------------------+---------------------+
| 1  | Lucile Stehr           | admin  | myrtle17@example.net          | Mosciski and Sons               | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 2  | Emilio Price           | admin  | donato.bogan@example.net      | Gleason LLC                     | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 3  | Milton Rutherford      | admin  | kody.bahringer@example.org    | Ebert, Rutherford and Homenick  | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 4  | Mr. Rogelio Swaniawski | admin  | josiane.baumbach@example.org  | Torphy, Gerlach and Leannon     | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 5  | Dr. Colten Maggio      | admin  | marianne.mitchell@example.org | O'Reilly, Schroeder and Kovacek | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 6  | Prof. Gail Spencer     | vendor | sgerlach@example.com          | Greenfelder, McGlynn and Zemlak | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 7  | Deion Pouros           | vendor | qcrist@example.com            | Strosin, Hodkiewicz and Torp    | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 8  | Dr. Sonny Deckow       | vendor | hlemke@example.net            | Bernhard PLC                    | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 9  | Karlie Beer            | vendor | harber.angelica@example.org   | Ritchie, Beer and Kihn          | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 10 | Madelynn Reinger III   | vendor | berta95@example.net           | Tremblay, Dooley and Hirthe     | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 11 | Neil Beier MD          | buyer  | stephan64@example.net         | Klein-Reichel                   | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 12 | Prof. Eino Bosco       | buyer  | schuster.michel@example.net   | Farrell-Schimmel                | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 13 | Hailey Quitzon Jr.     | buyer  | kiel.nienow@example.net       | Ward-Cremin                     | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 14 | Mrs. Ariane Grant      | buyer  | white.clay@example.net        | Spinka Ltd                      | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
| 15 | Ali Labadie            | buyer  | glenda73@example.org          | Raynor PLC                      | 2019-10-02 17:03:27 | 2019-10-02 17:03:27 |
+----+------------------------+--------+-------------------------------+---------------------------------+---------------------+---------------------+





For results of Part A - string parser with API 
7. php artisan serve
8. in a browser open url http://localhost:8000/api/just-parse?str=elapsed_time=0.0022132396697998047, type-CNC,radius-1-15,position-1=0.000000000000014,position-1//90,position-2=0%direction-1=-2.0816681711721685e-16

the result has to be JSON:
{"data":{"features":{"1":{"radius":"15","position":[0,90]},"2":{"position":[0]}},"elapsed_time":"0.0022132396697998047","feature_count":2,"type":"CNC"}}

Regards,
Tom




# Code Test

## Part A

Given a string with mixed delimiters and unknown order as input, return a JSON string as the output. 

Output should be structured and values should be type coerced and rounded as shown below in the *Example output*. 
The exact values shown are not representative of the example input.

Please document any assumptions and limitations with your given solution.

##### Example Input:
```
elapsed_time=0.0022132396697998047, type-CNC,radius-1-15,position-1=0.000000000000014,position-1//90,position-2=0%direction-1=-2.0816681711721685e-16
```

##### Example Output:
```json
{
  "data": {
    "features": [
      {
        "id": "163243",
        "radius": 43,
        "direction": -1.0415681311891685e-16,
        "position": [0, 90, 90]
      },
      {
        "id": "326477",
        "position": [0]
      }
    ],
   "elapsed_time": 0.001581,
   "type": "cnc",
   "feature_count": 2
  }
}
```

You can complete the above test in your choice of language. Bear in mind internally we use PHP (Laravel), Python (Flask)
 & JavaScript (Vue).
 
 You will be marked based on your adherence to the example output and the robustness of your answer. 
 To allow us to get a good understanding of your professional work please provide a solution you would be happy for a 
 colleague to continue to maintain and scale.

### Extension

Code, or describe how you would, implement your given solution as an API endpoint.

## Part B

Given the following tables, using a MySQL Query, produce the result shown in the *Desired Output*.

##### Desired Output:

Result Table:
```
id      name                role    email                   company_name        registered_on           last_login
4       Myrtis Klein        admin   myrtis@yahoo.com        Wilderman-Heller    2018-07-15 13:08:57     2019-09-13 09:42:30
3       Nelson Powlowski    vendor  nelson.pow@gmail.com    Kertzmann LLC       2018-05-29 15:14:50     2019-09-13 09:42:30
2       Candelario Rempel   buyer   c.rempel@hotmail.com    Mertz-Bradtke       2018-04-20 14:59:21     2019-09-13 09:42:30
1       Viva Ratke          buyer   viva.ratke@gmail.com    Hartmann-Wiegand    2018-04-08 16:53:43     2019-09-13 09:42:30
```

##### Provided Tables

Note that you are not allowed to add additional records to the tables.

Users Table:
```
id      name                email                   password            created_at           last_login
1       Viva Ratke          viva.ratke@gmail.com    $2y$10$7j8Wcokg     2018-04-08 16:53:43  2019-09-13 09:42:30
2       Candelario Rempel   c.rempel@hotmail.com    $2y$10$7j8Wcokg     2018-04-20 14:59:21  2019-09-13 09:42:30
3       Nelson Powlowski    nelson.pow@gmail.com    $2y$10$7j8Wcokg     2018-05-29 15:14:50  2019-09-13 09:42:30
4       Myrtis Klein        myrtis@yahoo.com        $2y$10$7j8Wcokg     2018-07-15 13:08:57  2019-09-13 09:42:30
```

Profiles Table
```
id      user_id     company_name        created_at 
1       1           Hartmann-Wiegand    2018-04-08 16:53:43
2       2           Mertz-Bradtke       2018-04-20 14:59:21
3       3           Kertzmann LLC       2018-05-29 15:14:50
4       4           Wilderman-Heller    2018-07-15 13:08:57
```

Roles Table
```
id      name     created_at 
1       admin    2017-04-08 16:53:43
2       vendor   2017-04-20 14:59:21
```

Model Has Roles Table
```
role_id     model_type          model_id    created_at
1           App\Models\User     1           2017-04-08 16:53:43
2           App\Models\User     2           2017-04-20 14:59:21
```

### Extension

Code, or describe how you would, implement your given solution as a response to a CLI command.
