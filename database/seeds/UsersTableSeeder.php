<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Spatie\Permission\Models\Role::all() as $role) {
            $users = factory(App\User::class, 5)->create()->each(function($user) use ($role){
                $user->assignRole($role);
                $user->userProfile()->save(factory(App\UserProfile::class)->make(['user_id' => $user->id]));
            });
         }
    }
}
