<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // reset
        app()[Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permission
        Permission::create(['name' => 'hasAccessToGoldenCave']);
        Permission::create(['name' => 'canSell']);
        Permission::create(['name' => 'canBuy']);

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('hasAccessToGoldenCave');

        $role = Role::create(['name' => 'vendor']);
        $role->givePermissionTo('canSell');

        $role = Role::create(['name' => 'buyer']);
        $role->givePermissionTo('canBuy');



    }
}
